# Windows Share on Unix



## Pre-reqs

1.   Check if cifs is installed

   ```bash
   sudo su - root
   
   yum list installed | grep cifs
   ```

   Which should list something like this:

   ```bash
   cifs-utils.x86_64           6.2-10.el7         @rhel
   ```

   

2.   If nothing was listed above then install cifs

   ```bash
   sudo su - root
   
   yum install cifs-utils
   ```

   

## Create a mountpoint

Before mounting the shared drive either temporarily or permanently we need to create a mountpoint to attach it to e.g.

```bash
mkdir /windowshare
```



## Manually mounting the share

To mount the windows share manually we can use the mount.cifs command as root.   This will give you access to the shared folder, but will need to be run again after a system reboot



```bash
mount.cifs  //<SHARED_FOLDER_PATH>    /<MOUNTPOINT>   -o user=<WINDOWS_USER>,uid=<LINUX_USER>
```

e.g.

```bash
mount.cifs //172.31.31.100/ftproot /windowshare -o user=sa_swiftalliance,uid=ec2-user
```



You will at this point be prompted for the password for the windows user.   Once entered successfully, you should be able to see your shared folder in /windowshare



## Automounting the share

As seen from the step above we need to pass in the password of the windows user.  Whilst this could be done in the command we are about to add into the fstab file, this isn't very secure.  The more secure option would be to create a hidden  credentials file in the root home directory

```bash
sudo su - root

vi ~/.credentials
```



```bash
username=sa_swiftalliance
password=somesupersecurepassword
```



We then need to update the fstab file  (take a backup first)

```bash
sudo su - root

cd /etc

cp fstab fstab.bkup

vi fstab
```

Then add the following to the end of the file

```bash
//<SHARED_FOLDER_PATH>  /<MOUNTPOINT> cifs uid=<LINUX_USER>,credentials=/root/.credentials,iocharset=utf8,vers=2.1 0 0
```

e.g.

```bash
//172.31.31.100/ftproot /windowshare cifs uid=ec2-user,credentials=/root/.credentials,iocharset=utf8,vers=2.1 0 0
```



## Testing automount

If the shared file is already mounted manually, unmount it

```bash
sudo su - root

umount /windowshare
```

Then validate the fstab by running

```bash
sudo su - root

mount -a
```

Any unmounted files listed in the fstab should now be mounted.  Double check by listing the contents of the /windowshare directory so ensure the files you can see on your windows machine are visible.







